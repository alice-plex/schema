# Changelog

## 3.1.2

* Add `normalize_title`

## 3.1.1

* Fix movie's original_title

## 3.1.0

* Change original_title to list

## 3.0.3

* Fix replace dot edge case

## 3.0.2

* Update repository
