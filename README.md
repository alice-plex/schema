# aliceplex-schema

aliceplex-schema is a schema library for Plex. It provides basic Plex related model.
This can be used to develop other Plex library.

## Install

```bash
pip install aliceplex-schema
```
